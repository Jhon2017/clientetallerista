package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class PanelLogin extends JPanel {

     
    public PanelLogin() {
        super();
    	JLabel labelUsername = new JLabel("Email: ");
        JLabel labelPassword = new JLabel("Clave: ");
        JTextField textUsername = new JTextField(20);
        JPasswordField fieldPassword = new JPasswordField(20);
        JButton buttonLogin = new JButton("Entrar");
        // create a new panel with GridBagLayout manager
        setLayout(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 0;     
        add(labelUsername, constraints);
 
        constraints.gridx = 1;
        add(textUsername, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 1;     
        add(labelPassword, constraints);
         
        constraints.gridx = 1;
        add(fieldPassword, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        buttonLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(textUsername.getText());
				System.out.println(fieldPassword.getPassword());
			}
		});
        add(buttonLogin, constraints);
         
        // set border for the panel
        setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Login Panel"));

    }
}
